
#include <stdio.h>

#include "MDP.h"



void MDP::computeQValue(State &s, const int action)
{


	// result of checking the next neighbor
	// i.e for north_check we check to see if it is out of bounds, diamond, wall, or pitfall
	float north_check = 0.0;
	float south_check = 0.0;
	float east_check = 0.0;
	float west_check = 0.0;

	// q values of neighboring action
	float north_value = 0.0;
	float south_value = 0.0;
	float east_value = 0.0;
	float west_value = 0.0;
	float total_q = 0.0; // the total q value of the taken action
	Point current_direction = s.location;

	if (action == 0) {
		// east - neighbors are north and south
		// check all directions to see if they're on diamond, pitfall or out of bounds
		east_check = check_state(s, ACTION_EAST);
		north_check = check_state(s, ACTION_NORTH);
		south_check = check_state(s, ACTION_SOUTH);

		// compute q value of action
		east_value = TRANSITION_SUCCEED * (ACTION_REWARD + (GAMMA * east_check));
		north_value = TRANSITION_FAIL * (ACTION_REWARD + (GAMMA * north_check));
		south_value = TRANSITION_FAIL * (ACTION_REWARD + (GAMMA * south_check));
		total_q = east_value + north_value + south_value;
		s.q_values[0] = total_q;
	}
	else if (action == 1) {
		// south - neighbors are west and east

		// check all directions to see if they're on diamond, pitfall or out of bounds
		south_check = check_state(s, ACTION_SOUTH);
		east_check = check_state(s, ACTION_EAST);
		west_check = check_state(s, ACTION_WEST);

		//compute q value of action
		south_value = TRANSITION_SUCCEED * (ACTION_REWARD + (GAMMA * south_check));
		west_value = TRANSITION_FAIL * (ACTION_REWARD + (GAMMA * west_check));
		east_value = TRANSITION_FAIL * (ACTION_REWARD + (GAMMA * east_check));
		total_q = east_value + west_value + south_value;
		s.q_values[1] = total_q;

	}
	else if (action == 2) {
		// west - neighbors are north and south
		// check all directions to see if they're on diamond, pitfall or out of bounds
		west_check = check_state(s, ACTION_WEST);
		north_check = check_state(s, ACTION_NORTH);
		south_check = check_state(s, ACTION_SOUTH);

		//compute q value of action
		west_value = TRANSITION_SUCCEED * (ACTION_REWARD + (GAMMA * west_check));
		north_value = TRANSITION_FAIL * (ACTION_REWARD + (GAMMA * north_check));
		south_value = TRANSITION_FAIL * (ACTION_REWARD + (GAMMA * south_check));
		total_q = west_value + north_value + south_value;
		s.q_values[2] = total_q;
	}
	else {
		// north - neighbors are west and east 
		// check all directions to see if they're on diamond, pitfall or out of bounds
		north_check = check_state(s, ACTION_NORTH);
		west_check = check_state(s, ACTION_WEST);
		east_check = check_state(s, ACTION_EAST);

		// compute q value of action
		north_value = TRANSITION_SUCCEED * (ACTION_REWARD + (GAMMA * north_check));
		west_value = TRANSITION_FAIL * (ACTION_REWARD + (GAMMA * west_check));
		east_value = TRANSITION_FAIL * (ACTION_REWARD + (GAMMA * east_check));
		total_q = north_value + west_value + east_value;
		s.q_values[3] = total_q;

	}

}

// determines the landing state value based on current action 
float MDP::check_state(State &s, const int action) {
	
	//east 
	if (action == 0) {
		if (s.location.x + 1 > 3) {
			// out of bounds
			return s.state_value;
		}
		else if (s.location.x + 1 == 1 && s.location.y == 1) {
			// wall
			return s.state_value;
		}
		else if (s.location.y == 0 && s.location.x + 1 == 3) {
			// diamond 
			return 1.0;
		}
		else if (s.location.x + 1 == 1 && s.location.y == 3) {
			// pitfall 
			return -1.0;
		}
		else {
			// regular action
			int x = int(s.location.x + 1);
			int y = int(s.location.y);
			return states[y][x].state_value;
		}
	}
	//west
	if (action == 2) {
		if (s.location.x - 1 < 0) {
			// out of bounds
			return s.state_value;
		}
		else if (s.location.x - 1 == 1 && s.location.y == 1) {
			// wall 
			return s.state_value;
		}

		else if (s.location.y == 0 && s.location.x - 1 == 3) {
			// diamond 
			return 1.0;
		}
		else if (s.location.x - 1 == 1 && s.location.y == 3)
		{
			// pitfall 
			return -1.0;
		}
		else {
			//regular action 
			int x = int(s.location.x - 1);
			int y = int(s.location.y);
			return states[y][x].state_value;
		}


	}
	//south
	if (action == 1) {
		if (s.location.y + 1 > 2) {
			// out of bounds
			return s.state_value;
		}
		else if (s.location.x == 1 && s.location.y + 1 == 1) {
			// wall 
			return s.state_value;
		}
		else if (s.location.y + 1 == 0 && s.location.x == 3) {
			// diamond
			return 1.0;
		}
		else if (s.location.y + 1 == 1 && s.location.x == 3) {
			// pitfall
			return -1.0;
		}
		else {
			// regular action
			int x = int(s.location.x);
			int y = int(s.location.y + 1);
			return states[y][x].state_value;
		}


	}
	//north
	if (action == 3) {
		if (s.location.y - 1 < 0) {
			// out of bounds
			return s.state_value;
		}
		else if (s.location.x == 1 && s.location.y - 1 == 1) {
			// wall
			return s.state_value;
		}
		else if (s.location.x == 0 && s.location.y - 1 == 3) {
			// diamond
			return 1.0;
		}
		else if (s.location.x == 1 && s.location.y - 1 == 3) {
			// pitfall
			return -1.0;
		}
		else {
			// regular action
			int x = int(s.location.x);
			int y = int(s.location.y - 1);
			return states[y][x].state_value;
		}
	}
}



void MDP::valueIteration()
{
	std::vector<float> q_changes;
	for (int y = 0; y < 3; y++) {

		for (int x = 0; x < 4; x++) {

			std::vector<int> temp = { y, x };

			// ignore wall, diamond and pitfall 
			if (temp != wall && temp != pit && temp != diamond) {

				computeQValue(states[y][x], ACTION_NORTH);
				computeQValue(states[y][x], ACTION_EAST);
				computeQValue(states[y][x], ACTION_SOUTH);
				computeQValue(states[y][x], ACTION_WEST);

				// state value is the highest q value from these 4 actions

				float max_value = states[y][x].q_values[0];

				for (int z = 0; z < 4; z++) {
					if (max_value < states[y][x].q_values[z]) {
						max_value = states[y][x].q_values[z];
					}

				}
				// append difference between current state q max and new state q max (for convergence)
				float difference = max_value - states[y][x].state_value;
				q_changes.push_back(difference);
				states[y][x].state_value = max_value;
			}
		}
	}
	// then update convergence value to state value with the biggest change (the max in the q_changes method)

	float max_change = *max_element(q_changes.begin(), q_changes.end());
	cur_convergence = max_change;

}




MDP::MDP()
{
	/*Initialize all the state with 0.0 state_value and 0.0 Q_values*/
	for (int y = 0; y < 3; y++)
	{
		for (int x = 0; x < 4; x++)
		{
			states[y][x].location.x = x; //specify the location for this state
			states[y][x].location.y = y;

			states[y][x].state_value = 0.0; //define the state value
			states[y][x].q_values[0] = 0.0; //define the Q value
			states[y][x].q_values[1] = 0.0;
			states[y][x].q_values[2] = 0.0;
			states[y][x].q_values[3] = 0.0;
		}
	}

	
	states[0][3].state_value = 1.0;
	states[1][3].state_value = -1.0;



	
	cur_convergence = 100;


}


MDP::~MDP()
{

}

void MDP::resetData()
{
	/*Initialize all the state with 0.0 state_value and 0.0 Q_values*/
	for (int y = 0; y < 3; y++)
	{
		for (int x = 0; x < 4; x++)
		{
			states[y][x].location.x = x; //specify the location for this state
			states[y][x].location.y = y;

			states[y][x].state_value = 0.0; //define the state value
			states[y][x].q_values[0] = 0.0; //define the Q value
			states[y][x].q_values[1] = 0.0;
			states[y][x].q_values[2] = 0.0;
			states[y][x].q_values[3] = 0.0;
		}
	}

	
	states[0][3].state_value = 1.0;
	states[1][3].state_value = -1.0;



	cur_convergence = 100; 


}
